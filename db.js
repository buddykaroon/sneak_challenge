var lorem =
	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ornare sapien a nisi egestas lacinia. Donec nibh felis, luctus eget dolor ac, aliquet vulputate tellus. Donec nec pretium tellus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.'

module.exports = {
	beers: [
		{
			id: 1,
			title: 'Singha',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://media-cdn.tripadvisor.com/media/photo-s/11/b1/b1/82/singha-beer-in-deutschland.jpg'
		},
		{
			id: 2,
			title: 'Chang',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://i.pinimg.com/originals/70/0b/70/700b7044384d4e3381db2b77451535d2.jpg'
		},
		{
			id: 3,
			title: 'Leo',
			description: lorem,
			style_id: 1,
			country_id: 1,
			image_path: 'https://www.thaizer.com/wp-content/uploads/2012/06/LeoBeer2Thaizer.jpg'
		},
		{
			id: 4,
			title: 'Hoegaarden',
			description: lorem,
			style_id: 2,
			country_id: 3,
			image_path: 'https://cfcdn2.azsg.opensnap.com/azsg/snapphoto/photo/LE/GWEN/3C6G521BD464B395B8DFD1lv.jpg'
		},
		{
			id: 5,
			title: 'Speedway Stout',
			description: lorem,
			style_id: 3,
			country_id: 2,
			image_path: 'https://cdn.shopify.com/s/files/1/0592/5937/products/alesmith-speedway-stout-can_grande.jpg?v=1571439137'
		},
		{
			id: 6,
			title: 'Furphy',
			description: lorem,
			style_id: 2,
			country_id: 4,
			image_path: 'https://s3-ap-southeast-2.amazonaws.com/www.beerandbrewer.com/wp-content/uploads/sites/2/2018/02/04102224/23244331_1558897174204864_697218774503026206_n-450x285.jpg'
		},
		
		{
			id: 7,
			title: 'Victorian Bitter',
			description: lorem,
			style_id: 3,
			country_id: 4,
			image_path: 'https://cdn.mryum.com/prod/a23a1002-3ad2-4a67-a112-e1ed6aaed192/image-md'
		}
	],
	favorite_beers: [{ id: 1, beer_id: 1 }, { id: 2, beer_id: 2 }],
	styles: [
		{ id: 1, name: 'Pale Lager' },
		{ id: 2, name: 'Wheat beer' },
		{ id: 3, name: 'Imperial Stout' }
	],
	countries: [
		{ id: 1, name: 'Thailand' },
		{ id: 2, name: 'United States' },
		{ id: 3, name: 'Germany' },
		
		{ id: 4, name: 'Australia' }
	]
}
