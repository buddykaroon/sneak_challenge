# SNEAK_Challenge

Challenge for Sneak Front-End position<br/>

Author: (Buddy) Chawanwit Karoonyavanich<br/>
Date: 7 / 2 / 2020<br/>

How to run challenge:<br/>
Open terminal and navigate to this folder, then run `npm install`, and then `npm run gql` to run the server.
Once the server is up, run `npm run start`.

### Final product walkthrough
A video showing the final product<br/>
#### https://youtu.be/4tSvrpom0N0 <br/>

Basic responsiveness is implemented, with CSS in styleConstants able to adapt to a smaller phone screen, creating a single
feed view similar to applications like Instagram. On full screen, I tried to make it mimic image boards such as Pinterest <br/>
### Approach 

The App's components are structured according to the entity relationship diagram below.<br/>
![Image of ERD](/images/entity_relationship_diagram.jpg)\
<i> Note: Heiarchy is represented top-down (i.e. App is root of all components, and Beers is parent to BeersColumn) </i><br/>
Information about each specific component can be found as comments in its respective .tsx file. <br/>

#### Flaws
Checkbox component determines the Country / Style filter that BeersColumns uses to display beers. Because of how distant 
these components are in the App heiarchy, the data needs to be sent from Checkbox up Navbar and up App, and then
passed down to Beers, and finally BeersColumn.<br/>

In hindsight, this is bad application design, but I did it mainly to keep Navbar as its own independent component seperate of Beers,
in the case of future expansion; where it can be used on other pages. <br/>

Another flaw arose when I made the Heart Buttons update together. I used a React hook setState() to refresh App.tsx whenever a mouse clicks,
meaning all corresponding Heart Buttons for a Beer update whenever one is pressed. However, the infinite scrolling component also refreshes, 
which means that it goes back to its starting position. This is evident when you scroll down and try to like a Beer.<br/>

## Known Bug [IMPORTANT]
Database Favorite issue as detailed by bug I emailed to Pack.

The bug is that removeFavoriteBeer(id) does not remove a favorited beer with the  specificed id, instead, removes 
only the most recently added favorite beer. 

i.e. If favorites were something like `[1,5,3,4,2]`<br/>
     and I ran `removeFavoriteBeer(3)`<br/>
     Favorites would result as `[1,5,3,4]`, instead of `[1,5,4,2]`<br/>
<br/>

My initial idea of a work around was:<br/>
  * (0) Given favorites `[1,5,3,4,2]`, and tasked to remove Beer `3`, meaning we want favorites to yield  `[1,5,4,2]`. The only tools we have to alter the database is `removeFavoriteBeer()` which is basically array.pop() and `addFavoriteBeer(id,beer_id)`, which works fine.<br/><br/>
  * (1) Find index of `3`. Since `3` is an ID, this means we can just grab the first `3` we encounter.<br/><br/>
  * (2) Create a `temp_array`, and store every element after `3`'s index. i.e. `temp_array = [4,2]`. This is NOT written to the database.<br/><br/>
  * (3) Call `removeFavoriteBeer()` as many times as the length of the `temp_array` plus one. Which is 3 times in our example. 
        Since removeFavoriteBeer only removes the first input, it will remove the first 3 favorited beers resulting in `[1,5]`*<br/><br/>
  * (4) For each element in `temp_array`, call `addFavoriteBeer(id: element, beer_id: element)`. 
        This will results in re-adding the elements in `temp_array` to the database, 
        yielding  the desired result of `[1,5,4,2]`<br/><br/>

 *Because this writes to the database, any component that queries favorites will also yield `[1,5]`. <br/>

Currently I have implemented up to step (3) of the work around. Step (4) is technically implemented, but
only updates when you actually click a 'like' button. This is due to how mutations work in React, and
how you cannot do two different mutate calls in a single form submit (as we are trying to remove then add data in the same button press). <br/>

Right now, the way to safely remove a 'favorited' beer is to unlike, then like it, and unlike it again.
This is because the initial unlike will remove all following favorited beers (step (3)), but liking it will update the database via step (4). And the final unlike is to remove the like just now.<br/>

