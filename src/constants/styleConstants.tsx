import styled from 'styled-components';
// Constants that use styled components and general CSS styling. Used throughout application. 

export const Dropdown = styled.div`
    transition-duration: 0.4s; 
`

export const Title = styled.h1`
                        color:blue;
                        font-size:100px`

export const HeightPadding = styled.div`
height: 200px;

@media only screen and (max-width: 700px) {
    height: 100px; 
}`

export const heartStyle = {
    width: '2rem',  
    top: "0",
    left: "0", 
}  

export const HeightPaddingSmall = styled.div`height: 90px`

export const buttonStyle = {
    backgroundColor: " rgba(0,0,0,0)", 
    border: "none",
    padding:" 15px  0px 0px 0px",
    display: "inline-block" 
}

export const BeersStyle : any = styled.div`
display:block;
margin:auto;
`

export const Card : any= styled.div`  
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;  
  padding: 32px 32px 32px 32px;
  background: rgb(240,240,210)
   `

export const Logo : any = styled.h3`
color:white;
transition: 0.3s;  
padding: 24px 24px 24px 24px;
@media only screen and (max-width: 700px) {
    padding: 32px 24px  0px 10px;
    font-size: 100%;
}
@media only screen and (max-width: 412px) {
    display:none
}
`

export const FavoriteIcons : any = styled.div`  
@media only screen and (max-width: 1100px) {
    display:none
}`

export const navHide : any = styled.div`
@media only screen and (max-width: 986px) {
    display: none;
}`

export const Item : any= styled.div`  
box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
transition: 0.3s;  
padding: 12px 12px;
margin: 12px 12px;
max-width:300px;
background: rgb(250,250,210);
@media only screen and (max-width: 700px) {
    max-width:400px;  
}
`

export const BeerImage : any = styled.div`  
width:400px;
display:block;
margin:auto;
transition: 0.3s;  
@media only screen and (max-width: 700px) { 
        width:300px;
        padding: 0px 0px 50px 0px
  }`

export const mainColumn : any = styled.div`  
display:block;
margin:auto;
transition: 0.3s;  
@media only screen and (max-width: 700px) { 
    width:100%;  
  }`

export const extraColumn : any = styled.div`
display:block;
margin:auto;  
transition: 0.3s;  
@media only screen and (max-width: 700px) {
            display: none;
  }`
  