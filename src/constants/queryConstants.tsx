 
import gql from 'graphql-tag'
//Constants used throughout the code
export const FAV_QUERY = gql`query{
    allFavoriteBeers{
        beer_id
    }
}`
export const COUNTRY_QUERY = gql`query{
    allCountries{
        id
        name
    }
}`
export const STYLE_QUERY = gql`query{
    allStyles{
        id
        name
    }
}` 

export const BEER_ADD = gql`
mutation CreateFavouriteBeer($id: ID!, $beer_id: ID!) { 
 
  createFavoriteBeer(id: $id, beer_id: $beer_id) {
    id
    beer_id
  } 
}`

export const BEER_DELETE = gql`
mutation RemoveFavouriteBeer($id: ID!) {
  removeFavoriteBeer(id: $id)  
} 
`