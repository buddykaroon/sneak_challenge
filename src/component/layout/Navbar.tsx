import React, { Component } from 'react'  
import { Query } from 'react-apollo';
import Checkbox from '../ui/checkbox'; 
import Favourites from '../ui/Favourites';
import * as GetQuery from "../../constants/queryConstants"
import * as styleConstant from "../../constants/styleConstants"
//Navbar.tsx------------------------------------------------------
//The Navbar element displays on both pages (about and main)
//Navbar contains information, such as current favorited beers 
//Navbar has two drop down elements : style and country. 
// These components create an array data structure that will be used
// to create the filter. 
//----------------------------------------------------------------
 
let filter : any[] = []
export class Navbar extends Component <{listener : any}, {}>  { 
    //Filter array is an array of arrays,
    // structured as  [[ id,CheckboxType, isChecked], ...]
    // That is, if the checkbox "Thailand" is unchecked, it would be
    // [1, Country, False]
    // The filter array contains one element for each checkbox. 
    // The filter array is sent up to App.tsx, and sent back down
    // to be unpacked and used to filter beers in BeersColumn.tsx
    updateFilter (data : any[])
    {  
        if (data.length === 3)
        {
        if (data[2] === true)
        {  
            filter.push([data[0], data[1]])
        }
        }
    } 
    render() {
        return (
            // Navbar Styling
            <div style = {{position: "fixed",    width:"100%", top: "-0px", zIndex: 3, backgroundColor : "rgb(200, 150, 50)"}}>
            {/* Get Favorites from database */}
            <Query query = {GetQuery.FAV_QUERY}>
            {({loading, error, data} :any) => {  
                    if (loading) return <h1>Loading</h1>
                    if (error) console.log(error)  
                    const favs: any[] = []
                    data.allFavoriteBeers.forEach((favBeer: any) => favs.push(favBeer.beer_id)) 
                    return <div></div>
                }}
            </Query>
            {/* Navbar  */}
            <ul style = {{background:"pink"}}>
                {/* 1) Beerify Logo */}
                
                <li >
                    
                    <styleConstant.Logo> B E E R I F Y 
                    </styleConstant.Logo>
                    </li>
                {/* 2) Filter by Country */}
                <li style={{float:"right"}} className="dropdown  py-3">
                    <div className="dropbtn p-3">Filter by Country</div> 
                    <div className="dropdown-content ">  
                    <Query query = {GetQuery.COUNTRY_QUERY}>
                    {({loading, error, data} :any) => {  
                            if (loading) return <h1>Loading</h1>
                            if (error) console.log(error)  
                            const countries: any[] = []
                            // For each Country in query, add a checkbox to the drop down.
                            data.allCountries.forEach((country: any) => countries.push(
                                <Checkbox 
                                boxType = "Country"
                                id = {country.id}
                                caption = {country.name}
                                update= {this.updateFilter}
                                initCheck = {true}/>)) 
                                this.props.listener(filter)
                            filter = [] 
                            return <div>{countries}</div>
                        }}
                    </Query>
                    </div>
                </li>
                {/* 3) Filter by Style */}
                <li style={{float:"right" }} className="dropdown py-3"> 
                        <div  className="dropbtn p-3" >Filter by Style</div>  
                    <div className="dropdown-content ">
                    <Query query = {GetQuery.STYLE_QUERY}>
                    {({loading, error, data} :any) => {  
                            if (loading) return <h1>Loading</h1>
                            if (error) console.log(error)  
                            const styles: any[] = []
                            // For each Style in query, add a checkbox to the drop down. 
                            data.allStyles.forEach((style: any) => styles.push(<Checkbox
                                boxType = "Style"
                                id = {style.id}
                                caption = {style.name} 
                                update= {this.updateFilter} 
                                initCheck = {true}/>))
                            return <div>{styles}</div>
                        }}
                    </Query>
                    </div>
                </li>
                {/* 4) Favorites tab */}
                <li  style={{float:"left", width:"50%"}}><Favourites/></li>
            </ul>   
            </div>
        )
    }
}

export default Navbar
