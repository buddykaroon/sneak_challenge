import React from 'react' 
import HeartButton from '../heart/HeartButton'
import { Link } from 'react-router-dom' 
import * as styledConstants from "../../constants/styleConstants"
// BeerItem.tsx ------------------------------------------------
// The cards on the home page that displays a single beer each.
// Each card shows an image, Beer name, and a heart button.
// Clicking on the image will send the user to the respective detail page.
// Clicking on the heart button will (un)like the beeer (See HeartButton.tsx)
// --------------------------------------------------------------
// Parent component to HeartButton.tsx
// Child component to BeerColumn.tsx

//Define Props [Self descript]
type CardProps = {
    id: string ,
    beerName: string,
    image_path : string,
    favorited: boolean
  } 

export const BeerItem = ({ id, beerName,  image_path, favorited}: CardProps) =>   
    <styledConstants.Item> 
        {/* Clickable Image */}
        <Link to= {`/${id}`}>   
            <img src={image_path} className=" card-img-bottom" alt = ""/>  
        </Link>
        {/* Name of Beer + Heart Button as a footer*/}
        <div className="row">
            <div className="col-9"> 
                <h3 className="card-title pt-3 text-justify">{beerName}  </h3>   
            </div>
            <div className="col-3 pt-1"> 
                <HeartButton initFav = {favorited} beer_id = {id}/>
            </div>
        </div>    
    </styledConstants.Item>
export default BeerItem
