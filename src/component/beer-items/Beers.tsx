import React from 'react' 
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import InfiniteScroll from 'react-infinite-scroll-component';
import Spinner from "../../img/spinner.gif" 
import * as styleConstant from "../../constants/styleConstants"
import BeersColumn from './BeersColumn';
//Beers.tsx---------------------------------------------------------
//Home page component. In charge of infinite scrolling, and creating
//Beer Columns of which contain the Beer items.
//------------------------------------------------------------------
//Parent component to BeersColumn.tsx
//Child component to App.tsx

const BEER_QUERY = gql`query BeerQuery {
    allBeers { 
        title
        id
        image_path
        country_id
        style_id
    }
  }
  `; 
  const FAVS_QUERY = gql`query BeerQuery {
    allFavoriteBeers { 
      beer_id
    }
  }
  `;   
  
class Beers extends React.Component <{filter : any}, {}> {
  state = {
    beers: Array.from({ length: 1 }),
    filter: this.props.filter 
  };
  fetchMoreData = () => {
    // Fake async Api Call for infinite scrolling that shows the loading spinner
    // for X miliseconds, before creating more beer columns.   
    setTimeout(() => {
      this.setState({
        beers: this.state.beers.concat(Array.from({ length: 1 }))
      });
    }, 1500);
  };  
  render() {
    
  const favs : any[] = []  
  return (  
        <div  >
            <styleConstant.HeightPaddingSmall/>
            <div  className = "d-flex flex-wrap justify-content-center  " style = {{ overflowX: "hidden", minHeight:"80rem", background:"rgb(220,200,150)"}} >
           
            <Query query = {FAVS_QUERY}>
            {({loading, error, data} :any) => {
                    if (loading) return <h1>Loading</h1>
                    if (error) console.log(error) 
                    data.allFavoriteBeers.forEach((favBeer: any) => favs.push(favBeer.beer_id) )
                   
                    return <div></div>
                     
            }}
            </Query>
            <Query query = {BEER_QUERY}>
                {({loading, error, data} :any) => {
                    if (loading) return <h1>Loading</h1>
                    if (error) console.log(error)
                    return (
                      <div className="" style = {{ width: "110rem" ,overflowX: "hidden"
                      }}>
          <InfiniteScroll
           dataLength = {this.state.beers.length} 
           next = {this.fetchMoreData} 
           hasMore = {true} 
           loader = {<img alt = "" src = {Spinner}/>}
           style = {{  overflowX: "hidden", minWidth:"1000px"}}> 
               
           {this.state.beers.map((i, index) => (
                      <div className="row px-3"  > 
                        <styleConstant.mainColumn>
                            <BeersColumn shuffle_index = {0.41438943438765685  + .5}
                             data = {data} favs = {favs} filter = {this.props.filter}></BeersColumn>
                        </styleConstant.mainColumn>
                         
                        <styleConstant.extraColumn>
                        <BeersColumn shuffle_index = {-0.04176448387423726 + .5} 
                             data = {data} favs = {favs} filter = {this.props.filter}></BeersColumn>
                        </styleConstant.extraColumn> 
                        <styleConstant.extraColumn>
                        <BeersColumn shuffle_index = {-0.2748982024859521  + .5}
                             data = {data} favs = {favs} filter = {this.props.filter}></BeersColumn>
                        </styleConstant.extraColumn> 
                        <styleConstant.extraColumn>
                
                        <BeersColumn  shuffle_index = {0.3069360426166958  + .5}
                             data = {data} favs = {favs} filter = {this.props.filter}></BeersColumn>
                             </styleConstant.extraColumn> 
                        <styleConstant.extraColumn>
                        <BeersColumn shuffle_index = {-0.37227884414356294  + .5}  
                             data = {data} favs = {favs} filter = {this.props.filter}></BeersColumn>
                             </styleConstant.extraColumn> 
                        </div>
                        
                        ))
                        }
             
          </InfiniteScroll> 
          </div>);
                }}
            </Query>
 
            </div>
        </div>  
);}}

export default Beers
