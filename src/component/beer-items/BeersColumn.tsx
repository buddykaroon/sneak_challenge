import React, { Component } from 'react'
import BeerItem from "./BeerItem" 
import * as styleConstant from "../../constants/styleConstants"
//BeersColumn.ts------------------------------------------------------------ 
// The vertical structure that contains one of each Beer from the database.
// Filtering is conducted on this level
//------------------------------------------------------------------------- 

//shuffle() : Helper function, a pseudorandom shuffle of an array given a seed
function shuffle(array: any[], shuffleSeed : number) {  
  let returnArray = array.slice()
  for(let i = returnArray.length - 1; i > 0; i--){
    const j = Math.floor(shuffleSeed * i)
    const temp = returnArray[i]
    returnArray[i] = returnArray[j]
    returnArray[j] = temp
  } 
return returnArray
}
export class BeersColumn extends Component <{shuffle_index : any, data : any, favs : any, filter : any},{}> {

  render() { 
    return ( 
      
      <styleConstant.BeersStyle>
        { 
          //Shuffle the array of beers, and only show beers that pass the filter test.
          shuffle(this.props.data.allBeers, this.props.shuffle_index).map((beer:any) => { 
          let countries: string[] = []
          let styles: string[] = [] 
          // Unpack the filter array into country and style filter arrays, respectively. 
          // New filter parameters (i.e. Price Range) can be added with a new if clause
          this.props.filter.forEach((element: string[]) => {
              if (element[1] === "Country")
              {
                countries.push(element[0])
              }
              else if (element[1] === "Style")
              {
                styles.push(element[0])
              }
          }); 
          //Check if both filters are satisfied, if so return the beer item
          if (countries.includes(beer.country_id) && styles.includes(beer.style_id))
          {
            return ( 
                <BeerItem 
                  id={beer.id}
                  beerName = {beer.title} 
                  image_path = {beer.image_path}
                  favorited = {this.props.favs.includes( beer.id ) ? true : false}/>
                ) 
          } 
        return (<div></div>)
        } )}
      
      </styleConstant.BeersStyle>
    )
  }
}

export default BeersColumn
