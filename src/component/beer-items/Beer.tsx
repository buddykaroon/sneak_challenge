import React, { Component } from 'react'
import { Query } from 'react-apollo'
import gql from 'graphql-tag';   
import { Link } from 'react-router-dom'; 
import * as styleConstants from '../../constants/styleConstants' 
//Beer.tsx ----------------------------------------------------------------
//Beer Component is the 'detail' page, and opens when an image is clicked. 
//Displays Beer information, such as name, type, country, description. 
//Contains a back button to go to home page. 
//------------------------------------------------------------------------- 
// Child component to App.tsx

//Helper Functions:
//getBeerQuery() : Deduces beer_id from the Router path
function getBeerQuery () {
    var beerID = window.location.pathname;  
    beerID = beerID.slice(1, 2);
    return gql`query BeerQuery{
        Beer (id :${beerID}){ 
          title
          id
          country_id
          style_id
          description
          image_path
        }
      }      
      `;  
} 
//getCountryQuery() : Query to get Country name, given its ID
function getCountryQuery (countryID : any) {
    return gql`{
        Country (id:${countryID}) { 
            name
        }
      }`
}
//getStyleQuery() : Query to get Style name, given its ID
function getStyleQuery (styleID : any) {
  return gql`{
      Style (id:${styleID}) { 
          name
      }
    }`
}
class Beer extends Component {  
    render() {  
        return (
            <div className="container"> 
            <styleConstants.HeightPadding/> 
              <styleConstants.Card>
                <Query query = {getBeerQuery()}>
                {({loading, error, data} :any) => {
                    if (loading) return <h1>Loading</h1>
                    if (error) console.log(error)  
                    return (
                        <div className="row ">
                            {/* Display Image on left column */}
                            <div className="col">  
                            
                                <styleConstants.BeerImage>
                                <img src={data.Beer.image_path} alt = "" style = {{width:"100%"}}/> 
                                </styleConstants.BeerImage>
                            </div>
                            {/* Display Beer properties on right column */}
                            <div className = "col">
                                <h1>{data.Beer.title}</h1>  
                                {/* Translate country_id and style_id to name strings */}
                                <Query query = {getCountryQuery(data.Beer.country_id)}>
                                {({loading, error, data} :any) => {
                                    if (loading) return <h1>Loading</h1>
                                    if (error) console.log(error) 
                                    return <h2>Country:{data.Country.name}</h2>}}
                                </Query>
                                <Query query = {getStyleQuery(data.Beer.style_id)}>
                                {({loading, error, data} :any) => {
                                    if (loading) return <h1>Loading</h1>
                                    if (error) console.log(error)  
                                    return <h2>Style: {data.Style.name}</h2>}}
                                </Query>  
                                <p> {data.Beer.description} </p>  
                                <Link to = "/" className = "btn btn-block btn-dark">Back</Link>
                            </div>
                          </div>
                      );
                }}
                </Query>
                </styleConstants.Card>
            </div>);
        }
    }

export default Beer