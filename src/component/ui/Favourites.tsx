import React, { Component } from 'react' 
import { Query } from 'react-apollo';
import BeerThumbnail from './BeerThumbnail';
import * as getQuery from "../../constants/queryConstants"
import * as styleConstants from "../../constants/styleConstants"
//Favorites.tsx----------------------------------------------------
//This component reads the amount of favorites from the database, and displays it
//in form of both numerical value and thumbnails of each beer. 
//-----------------------------------------------------------------
//Child component of Navbar
//Parent component of BeerThumbnail

const fav_style = {
    color:"white" 
}
export class Favourites extends Component {    
    render() {
        return (
            <styleConstants.navHide>
            <div style = {fav_style} className = "p-3">
                {/* Get Favorites */}
                <Query query = {getQuery.FAV_QUERY}>
                {({loading, error, data} :any) => {  
                    if (loading) return <h1>Loading</h1>
                    if (error) console.log(error)  
                    const favs : any[] = []  
                    //For Each favorite beer, add a thumbnail
                    data.allFavoriteBeers.forEach(
                        (favBeer: any) => favs.push(<BeerThumbnail beerID =  {favBeer.beer_id}></BeerThumbnail>)
                    ) 
                    return <div className="row" >
                                {/* Left column: Number of favorites */}
                                <div className="column py-3" style = {{minWidth: "250px"}}>
                                    <i> You have {favs.length}  Favourite Beers! </i>
                                </div>
                                <div className="column">
                                {/* Right column: Thumbnails */}<styleConstants.FavoriteIcons> 
                                <div className="flex-container row " style = {{display:"flex", flexWrap: "nowrap"}}>
                                    
                                {favs}
                                </div></styleConstants.FavoriteIcons>
                            </div>
                        </div>

                }}
                </Query>
            </div></styleConstants.navHide>
        )
    }
}

export default Favourites
