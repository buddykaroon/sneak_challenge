import React, { Component } from 'react'
import gql from 'graphql-tag'
import { Query } from 'react-apollo'
import Heart from "../../img/heart_filled.png"
//BeerThumbnail.tsx----------------------------------------
//Simple thumbnail with a heart icon showing liked beers.
//---------------------------------------------------------
//Child of Favorites 
const thumbnailStyle = { 
        width: "40px",
        height: "40px",
        backgroundPosition:" center center",
        backgroundRepeat: "no-repeat "  
}
const HeartThumbnailStyle = { 
    width: "20px",
    height: "20px",
    backgroundPosition:" center center",
    backgroundRepeat: "no-repeat " 
}

export class BeerThumbnail extends Component <{beerID : any}, {}> {
    //Get Custom image path depending on id
    getImagePath = gql`{
        Beer (id : ${this.props.beerID}){
          image_path
        }
      }`
    render() {
        return (
            <div className = "column">
                   <Query query = {this.getImagePath}>
                    {({loading, error, data} :any) => {  
                            if (loading) return <div></div>
                            if (error) console.log(error)    
                            return <div> 
                                        <div className="" style = {{maxHeight : "20px"}}>
                                            {/* Actual Beer photo */}
                                            <img className = "rounded-circle " style = {thumbnailStyle} src={data.Beer.image_path} alt=""/>  
                                            {/* Little heart icon */}
                                            <div style = {{ position: "relative" ,
                                                left: "4px",
                                                top: "-20px"}}>
                                                <img  style = {HeartThumbnailStyle}src={Heart} alt=""/>
                                            </div>
                                        </div> 
                                    </div>
                  
                    }}
                    </Query>
            </div>
        )
    }
}

export default BeerThumbnail
