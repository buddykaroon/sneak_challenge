import React, { Component } from 'react'
import CheckboxUnticked from "../../img/checkbox.png"
import CheckboxTicked from "../../img/checkbox_ticked.png"
//Checkbox.tsx-----------------------------------------------------
//Toggleable buttons that drop down from Filter tabs on the Navbar.
//Clicking these buttons will cause their respective filter to be true/false,
//of which is sent down the pipeline to the BeersColumn.tsx for 
// the correct filters to be applied and rendered.
//-----------------------------------------------------------------

type CheckboxProps = 
    {
    //InitCheck is the initial stage of the checkbox (true for checked, false for unchecked)
    initCheck : boolean, 
    caption : string,
    id : string, 
    //Box type is either "Country" or "Style"
    boxType : string,
    //Update is a function called in Navbar, used to send the checked or not data 
    update : any }

export class checkbox extends Component <CheckboxProps, {checked : boolean}> {
    constructor(props : any){
        super(props);
        this.state =  { checked: this.props.initCheck }; 
    }
    // Inverts the checked value
    setChecked () {
        if (this.state.checked)
        { 
            this.setState({checked : false}) 
        }
        else
        { 
            this.setState({checked : true})
        } 
    }
    render() {
        // Call the update to inform Navbar
        this.props.update([this.props.id, this.props.boxType,this.state.checked])
        return (
            <div className = "btn btn-light w-100" onClick = {() => 
                { this.setChecked() }} >
                {/* Button and Caption */}
                <img  src={this.state.checked ? CheckboxTicked : CheckboxUnticked} style = {{width:"30px", float:"left"}} className = "pr-3"alt=""/>
                <div style={{float:"right" }}>{this.props.caption}</div>
            </div>
        )
    }
}

export default checkbox
