import React, { Component } from 'react' 
import HeartEmpty from '../../img/heart.png'
import HeartFill from '../../img/heart_filled.png' 
import { Mutation, Query } from 'react-apollo'
import * as getQuerys from "../../constants/queryConstants"
import * as styleConstants from "../../constants/styleConstants"
import gql from 'graphql-tag'  

// HeartButton.tsx ----------------------------------------------
// The Heart Button placed on each Beer item card. Clicking this
// will like or unlike the respective Beer. 
// ***************KNOWN ISSUE***********************************
// Mutate Component doesn't delete specfic ID.
// --------------------------------------------------------------
// Child Component to BeerItem.tsx
 
type HeartProps = {
    initFav: boolean 
    beer_id : string 
}

// Arrays as detailed in README.md
let favs: string[] = []
let allFavs : string[] = []  
let reAdd : any[] = [] 

//eliminateKey() : Helper function that just removes a specific element from an array
function eliminateKey (favs : string[], key : any) {
  var i;
  for (i = 0; i < favs.length; i++) {
    if (favs[i] === key)
    {
      favs.splice(i,1)
    }
  } 
}

export class HeartButton extends Component <HeartProps, {  favorited : boolean}> {
  click : boolean = false 
  constructor(props : any) 
  { 
      super(props); 
      if (reAdd.includes(this.props.beer_id))
      { 
        this.state = { favorited : true } 
      }
      else
      { 
        this.state = { favorited : this.props.initFav } 
      } 
  } 
  //Runs when the button is clicked.
  //Changes the favorites array and 
  ClickButton (favoriteed : boolean) { 
    
    if (favoriteed) 
    {
      allFavs.push(this.props.beer_id) 
    } 
    else
    {
      eliminateKey(allFavs, this.props.beer_id)
    } 
    //Since all buttons are updated when the app refresh, 
    //Click boolean is used let the button know to ignore an App refresh if it is clicked
    this.click = true 
    this.setState({favorited : !favoriteed})
    this.setClickTimeout() 
  }   
  //Resets the Click boolean to false
  setClickTimeout () {  
    setTimeout(() => {
      this.click = false  
    }, 1); 
  }  
  render() {
    return( 
      <div> 
      {/* Getting all Favorites */}
      <Query query = {getQuerys.FAV_QUERY}>
          {({loading, error, data} :any) => {  
                  if (loading) return <div></div>
                  if (error) console.log(error)  
                  favs = []  
                  data.allFavoriteBeers.forEach((favBeer: any) => favs.push(favBeer.beer_id)) 
                  if (this.state.favorited !== this.props.initFav && !this.click)
                  {  
                    if (!reAdd.includes(this.props.beer_id)) 
                    {
                      this.setState({favorited : this.props.initFav}) 
                      this.click = false
                    }
                  } 
                  return <div></div>
              }}
      </Query>
      {
      !this.state.favorited ?  
      // If Beer is liked, we want to display the unlike button. 
        <Mutation mutation = {getQuerys.BEER_DELETE}>
          {(removeFavoriteBeer: any) => (
          <div>
            <form
              onSubmit={e => { 
                e.preventDefault(); 
                //Code version of the 'work around' in README.md
                let bIndex =favs.findIndex((beer : any) => {return (beer === (this.props.beer_id)) })
                let Beerindex = favs.length - bIndex
                let leftOverFavs = favs.slice(bIndex + 1, favs.length) //Re add it
                var i : number
                for (i = 0; i < Beerindex; i ++)
                {
                  removeFavoriteBeer({ variables: { id: this.props.beer_id },
                    refetchQueries: [{ query : gql` query AllBeers {
                      allFavoriteBeers {
                        id
                        beer_id
                      }
                    }`}]},
                    )  
                }
                //reAdd is the temp_array in this case.
                reAdd = []
                for (i = 0; i < leftOverFavs.length; i++)
                { 
                  reAdd.push(leftOverFavs[i]) 
                } 
              }}
            > 
            {/* Actually rendering the button */}
            <button type="submit" className = "" onClick = {(() => this.ClickButton (this.state.favorited))} style = {styleConstants.buttonStyle}>
                <img src={HeartEmpty} alt = "" className="p-1"   style = {styleConstants.heartStyle}/> 
            </button>
            </form> 
          </div>
          )}

        </Mutation>
      : 
      // else Beer is unliked, so we want to display the like button. 
        <Mutation mutation = {getQuerys.BEER_ADD}>
        {(createFavoriteBeer: any) => (
            <div>
              <form 
                onSubmit={e => {
                  e.preventDefault();
                  if (!favs.includes( this.props.beer_id ) )
                  {    
                        // This for loop is step (4) So far I can only get it to work under this mutation clause.
                        // Because it is here, it only triggers when you 'like' a beer. 
                        for (var i = 0; i < reAdd.length; i ++)
                        {
                          if (!favs.includes( reAdd[i] ) )
                          {    
                            createFavoriteBeer({ variables: { id: reAdd[i]  , beer_id: reAdd[i] } });
                          } 
                        }
                        reAdd = []   
                        createFavoriteBeer({ variables: { id: this.props.beer_id  , beer_id: this.props.beer_id },
                          refetchQueries: [{ query : gql` query AllBeers {
                            allFavoriteBeers {
                              id
                              beer_id
                            }
                        }`}] })  
                    }
                }}> 
                {/* Actually rendering the button */}
                <button type="submit" style = {styleConstants.buttonStyle} onClick = {(() => this.ClickButton (this.state.favorited))}>
                <img src={HeartFill} alt = "" style = {styleConstants.heartStyle}/> 
                </button> 
              </form>
            </div>
            )}
        </Mutation> 
      }   
    </div>
  )  
  }
} 
export default HeartButton
