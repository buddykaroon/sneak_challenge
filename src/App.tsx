import React, {useState, useEffect} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom'; 
import Beer from './component/beer-items/Beer'
import Beers from './component/beer-items/Beers'
import Navbar from './component/layout/Navbar'
import ApolloClient from "apollo-boost"; 
import { ApolloProvider } from 'react-apollo'; 
import './App.css';  
//App.tsx------------------------------------------------------------------ 
//The main script of the application. 
//Main functionality is to refreshes its child components to keep them up
//to date on each update. 
//------------------------------------------------------------------------- 
//Parent component of Beer, Beers, and Navbar

//Begin Apollo Client at the designated local host server
const client = new ApolloClient({
  uri: "http://localhost:3001/"
});

const App: React.FC = () => {
  
  //App uses useState to refresh its children components, the actual variable dummy is insignificant. 
  const [, setDummy] = useState([]); 

  //useEffect calls on starting application. Refreshing to get initial beers.
  useEffect(() => { 
    setTimeout(() => {
      setDummy([])   
    }, 50);  
    }, [])

  // Updates all components
  function refresh () { 
      setTimeout(() => {
        setDummy([]) 
        //A second refresh is called to yield filter updates 
        secondRefresh() 
      }, 50);  
  } 
  function secondRefresh () { 
    // Updates all hearts
    setTimeout(() => {
      setDummy([])  
    }, 100);  
  }  
  //Filter array is set by Navbar, and passed into Beers    
  let filter : any[] = []
  return (
    <ApolloProvider client = {client}> 
      <div className="App " style = {{ overflowX: "hidden" }} onClick = {refresh}> 
        <Router>
          {/* Navbar */}
          <Navbar listener = {(data:any) => (filter = data)}/>  
          {/* Home page Route (Beers)*/}
          <Route exact path = "/" component ={() => <Beers filter={filter} />}></Route>
          {/* Detail page Route (Beer)*/}
          <Route exact path = "/:beerID" render = {(props) => (
            <Beer {...props} /> )}></Route> 
        </Router>
      </div>
    </ApolloProvider>
  );
}

export default App;
